using System;
using System.ComponentModel.DataAnnotations;

namespace registration.Models
{
    public class StudentModel
    {
        [Key]
        public int id {get; set;}

        [StringLength(8, ErrorMessage = "This field is required")]
        [Required]
        public string FirstName { get; set; }

        [StringLength(8, ErrorMessage = "This field is required")]
        [Required]
        public string LastName { get; set; }

        [StringLength(8, ErrorMessage = "This field is required")]
        [Required]
        public string MidName { get; set; }

        [StringLength(8, ErrorMessage = "This field is required")]
        [Required]
        public string Gender { get; set; }

        [StringLength(8, ErrorMessage = "This field is required")]
        [Required]
        public string Address { get; set; }

        [Range(5,50, ErrorMessage = "Please Enter Valid Age")]
        [Required]
        public int Age { get; set; }

    }
}

