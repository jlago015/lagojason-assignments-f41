﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    public class UserDataModel
    {
        public Guid id { get; set; }
        public String playerName { get; set; }
        public int score { get; set; }
    }
}
