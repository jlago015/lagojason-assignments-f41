﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using backend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private DataContext _dataContext;

        public GameController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        [HttpPost()]
        public IActionResult saveData([FromBody]UserDataModel userData)
        {
            userData.id = Guid.NewGuid();
            _dataContext.UserData.Add(userData);
            _dataContext.SaveChanges();
            return Ok();
        }

        [HttpGet()]
        public IEnumerable getScores(){
            var query = from j in _dataContext.UserData
            orderby j.score descending
            select new {
                id = j.id,
                name = j.playerName,
                scr = j.score
            };
            return query;
        }
    }
}
